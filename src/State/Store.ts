import { createStore, combineReducers, applyMiddleware } from "redux";

//middlewares
import ReduxThunk from "redux-thunk";


//reducers
import { countReducer } from './Counter';
import { todosReducer } from './Todos';


const reducers = combineReducers({
    count: countReducer,
    todos: todosReducer
})

export type RootState = ReturnType<typeof reducers>

export default createStore(reducers, applyMiddleware(ReduxThunk));
