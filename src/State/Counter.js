//ACTION
export const addCount = (amount = 1) => {
    console.log('addCount');
    return {
        type: 'ADD_COUNT',
        payload: amount
    }
}


//REDUCER
export const countReducer = (count = 0, action) => {
    switch (action.type) {
        case 'ADD_COUNT':
            return count + action.payload;
        default:
            return count;
    }
}

