import axios from 'axios';

//actions
export const getTodos = () => {

    return async (dispatch) => {
        try {
            var resp = await axios.get('https://jsonplaceholder.typicode.com/todos');

            dispatch({
                type: 'GET_TODOS',
                payload: resp.data.slice(0, 10)
            })

        } catch (error) {
            console.log(error);
        }
    }
}

export const patchTodos = (item) => {
    return {
        type: 'PATCH_TODOS',
        payload: item
    }
}


//reducer 
export const todosReducer = (list = [], action) => {
    switch (action.type) {
        case 'GET_TODOS':
            return action.payload;
        case 'PATCH_TODOS':
            const index = list.findIndex(i => i.id === action.payload.id);
            list[index].completed = !action.payload.completed;
            console.log('PATCH_TODOS', list, index);
            return [...list];
        default:
            return list;
    }

}

