import React, { useReducer, useCallback, useState, useMemo } from 'react';
import { Box, Btn } from "UIKit";

import Title from 'Components/Title';


const reducer = (state: any, action: any) => {
    switch (action.type) {
        case 'INCREASE':
            return state + 1;
        case 'DECREASE':
            return state - 1;
        default:
            return state;
    }
}


const Hooks: React.FC = (props) => {
    const [count, action] = useReducer(reducer, 0);
    const [count2, setCount2] = useState(0);

    const longFunction = (count2: number) => {
        let newCount = count2;
        for (let i = 0; i < 1000000000; i++) {
            newCount++;
        }
        return newCount;
    }

    const calcCount = useMemo(() => longFunction(count2), [count2]);

    const handleIncreaseCount2 = () => {
        setCount2(calcCount);
    }

    const someFunc = useCallback(() => {
        console.log('hello mosh');
    }, [props.children])

    return (
        <Box>
            <Title title="Hello" func={someFunc} />
            <h2>Count, {count} -- Count2, {count2}</h2>
            <div>
                <Btn onClick={() => { action({ type: 'DECREASE' }) }}>-</Btn>
                <Btn onClick={() => { action({ type: 'INCREASE' }) }}>+</Btn>
            </div>
            <div>
                <Btn onClick={handleIncreaseCount2}>Add Count 2</Btn>
            </div>
        </Box>
    )
}


export default Hooks;