import React from 'react';
import { Box, Input } from "UIKit";


import TodosList from 'Components/TodosList';

import { useInput } from 'Hooks/useInput';

const Todos: React.FC = (props) => {
    const username = useInput();

    return (
        <Box>
            <TodosList />
            <Input {...username} />
        </Box>
    )
}


export default Todos;