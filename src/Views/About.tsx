import React, { useState } from 'react';
import { Box, Dropdown, Btn } from "UIKit";

const list = [
    { id: 1, value: 'select 1' },
    { id: 2, value: 'select 2' },
    { id: 3, value: 'select 3' },
    { id: 4, value: 'select 4' }
]

const About: React.FC<Props> = (props) => {
    const [selected, setSelected] = useState<number | null>(null);

    const handleOnChange = (selectedId: number) => {
        setSelected(selectedId);
    }

    const handleShow = () => {
        console.log(selected);

    }
    return (
        <Box>
            <h2>Dropdown</h2>
            <Dropdown list={list} onChange={handleOnChange} selected={selected} />
            <Btn onClick={handleShow}>Show Selected</Btn>
        </Box>
    )
}

interface Props {
    mosh: string
}

export default About;