import React from 'react';
import { Box } from "UIKit";


import CounterDisplay from 'Components/CounterDisplay';
import CounterAdd from 'Components/CounterAdd';

const Redux: React.FC = (props) => {


    return (
        <Box>
            <CounterDisplay />
            <CounterAdd />
        </Box>
    )
}


export default Redux;