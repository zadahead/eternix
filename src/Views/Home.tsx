import React, { useEffect, useState } from 'react';
import { Box } from "UIKit";

import axios from 'axios';


const Home: React.FC<Props> = (props) => {
    const [list, setList] = useState<[]>();

    useEffect(() => {
        fetchList();
    }, [])

    const fetchList = async () => {
        const resp = await axios.get('https://jsonplaceholder.typicode.com/todos');
        setList(resp.data);
    }
    const renderList = () => {
        if (list) {
            return list.map((i: any) => (
                <div key={i.id}>{i.title}</div>
            ))
        }
        return 'loading...'
    }

    return (
        <Box>
            <h2>Inner Width</h2>
            {renderList()}
        </Box>
    )
}

interface Props {
    mosh: string
}

export default Home;