import React, { useState } from 'react';



const Toggler: React.FC = (props) => {
    //state
    const [isDisplay, setIsDisplay] = useState<boolean>(true);

    //life cycle


    //handlers
    const handleToggle = () => {
        setIsDisplay(!isDisplay);
    }


    return (
        <div>
            <h2>Toggler</h2>
            {isDisplay && props.children}
            <button onClick={handleToggle}>Toggle</button>
        </div>
    )
}

export default Toggler;