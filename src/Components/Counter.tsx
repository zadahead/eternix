
import React, { useState } from 'react';

//functional component
const Counter: React.FC = () => {
    console.log('RENDER');
    const [count, setCount] = useState<number>(0);

    const handleClick = () => {
        setCount(count + 1);
    }

    const style = {
        color: 'red'
    }

    return <h1 style={style} onClick={handleClick}>{`Count ${count}`}</h1>;
}

export const calc = (a: number, b: number) => {
    return a + b;
}

export default Counter;