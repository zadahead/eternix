import React, { useEffect, useState } from 'react';

const LifeCycleFunc: React.FC = () => {
    //console.log('render');

    const [count, setCount] = useState<number>(0);
    const [count2, setCount2] = useState<number>(0);

    //componentDidMount
    useEffect(() => {
        //console.log('componentDidMount');

        return () => {
            //console.log('componentWillUnMount');
        }
    }, []);

    //componentDidMount
    useEffect(() => {
        console.log('count componentDidUpdate', count);

        return () => {
            //console.log('count componentWillUnMount', count);
        }
    }, [count]);

    const handleCount = () => {
        setCount(count + 1);
    }

    const handleCount2 = () => {
        setCount2(count2 + 1);
    }

    return (
        <div>
            <h1 onClick={handleCount}>LifeCycleFunc, {count}</h1>
            <h1 onClick={handleCount2}>LifeCycleFunc2, {count2}</h1>
        </div>
    )
}

export default LifeCycleFunc;