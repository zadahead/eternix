
import { Btn } from 'UIKit'
import { useDispatch } from 'react-redux';

import { addCount } from 'State/Counter';

const CounterAdd = () => {
    const dispatch = useDispatch();

    const handleAdd = () => {
        dispatch(addCount(5));
    }

    return <Btn onClick={handleAdd}>Counter Add</Btn>
}

export default CounterAdd;