import React from 'react';
import { useSelector } from 'react-redux';

import { RootState } from 'State/Store';

const CounterDisplay: React.FC = () => {
    const count = useSelector((state: RootState) => state.count);
    console.log('STATE', count);

    return <h2>CounterDisplay, {count}</h2>
}

export default CounterDisplay;