import React from 'react';

class LifeCycleClass extends React.Component {
    state = {
        count: 0
    }
    componentDidMount = () => {
        console.log('componentDidMount');
    }

    componentDidUpdate = () => {
        console.log('componentDidUpdate');
    }

    componentWillUnmount = () => {
        console.log('componentWillUnmount');
    }

    handleCount = () => {
        console.log('handleCount');

        this.setState({
            count: this.state.count + 1
        })
    }

    render = () => {
        console.log('render');

        return <h1 onClick={this.handleCount}>LifeCycleClass, {this.state.count}</h1>
    }
}

export default LifeCycleClass;
