import React, { useState } from 'react';


const list = [
    { id: 1, name: 'mosh' },
    { id: 2, name: 'david' },
    { id: 3, name: 'daniel' }
]
const Users: React.FC = () => {

    const renderList = () => {
        return list.map(i => {
            return <li key={i.id}>{i.name}</li>
        })
    }

    return (
        <div>
            <h1>Users List:</h1>
            <ul>
                {renderList()}
            </ul>
        </div>
    )

}

export default Users;