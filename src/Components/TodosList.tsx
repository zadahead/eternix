import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import { RootState } from "State/Store";
import { getTodos, patchTodos } from "State/Todos";


const TodosList = () => {
    //state
    const dispatch = useDispatch();
    const todos = useSelector((state: RootState) => state.todos);

    //life cycle
    useEffect(() => {
        setTimeout(() => {
            dispatch(getTodos());
        }, 300);
    }, [])

    //handlers

    const handleUpdateComplete = (item: {}) => {
        dispatch(patchTodos(item))
    }
    console.log('todos -> ', todos);

    const renderList = () => {
        if (todos.length) {
            return (
                <ul>
                    {
                        todos.map((i: any) => {
                            return <li
                                key={i.id}
                                onClick={() => handleUpdateComplete(i)}
                                style={{ textDecoration: i.completed ? 'line-through' : 'none' }}>
                                {i.title}
                            </li>
                        })
                    }
                </ul>
            )
        }
        return <div>loading...</div>
    }

    return (
        <div>
            <h2>TodosList</h2>
            {renderList()}
        </div>
    )
}

export default TodosList;