import React from "react"

const Title: React.FC<Props> = (props) => {
    console.log(props);

    return <h1>{`This is a Title -> ${props.title}`}</h1>
}

interface Props {
    title: string,
    func: () => void
}

export default React.memo(Title)