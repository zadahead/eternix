import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import './index.css';

import { BrowserRouter as Router } from 'react-router-dom';

//redux
import { Provider } from "react-redux";
import store from "State/Store";


ReactDOM.render(
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>,
  document.getElementById('root')
);

