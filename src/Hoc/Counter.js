import { useState } from 'react';

const counter = (Component) => {
    //new component
    const NewComponent = () => {
        const [count, setCount] = useState(0);

        const onIncreaseCount = () => {
            setCount(count + 1)
        }

        return <Component count={count} onIncreaseCount={onIncreaseCount} />
    }

    return NewComponent;
}

export default counter;
