import React from 'react';


import { GridRows, Line, Between, Inner } from 'UIKit';


import './App.css';

import { NavLink, Route, Switch, Redirect } from 'react-router-dom';

import Home from 'Views/Home';
import About from 'Views/About';
import Redux from 'Views/Redux';
import Todos from 'Views/Todos';
import Hooks from 'Views/Hooks';

const App: React.FC = () => {

    return (
        <div className="App">
            <GridRows>
                <div>
                    <Inner>
                        <Between>
                            <div>logo</div>
                            <Line>
                                <NavLink to="/">home</NavLink>
                                <NavLink to="/redux">redux</NavLink>
                                <NavLink to="/hooks">hooks</NavLink>
                                <NavLink to="/todos">todos</NavLink>
                            </Line>
                        </Between>
                    </Inner>
                </div>
                <div>
                    <Switch>
                        <Route exact path='/' component={Home} />
                        <Route exact path='/redux' component={Redux} />
                        <Route exact path='/hooks' component={Hooks} />
                        <Route exact path='/todos' component={Todos} />

                        <Redirect to='/' />
                    </Switch>
                </div>
            </GridRows>
        </div>
    )
}


export default App;