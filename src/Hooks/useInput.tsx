import React, { useState } from 'react';

export const useInput = () => {
    console.log('useInput');

    const [value, setValue] = useState('');

    const onChange = (value: string) => {
        console.log('setValue');

        setValue(value);
    }

    return {
        value,
        onChange
    }
}