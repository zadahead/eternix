import { useEffect, useState } from 'react';

export const useWidth = () => {
    const [width, setWidth] = useState<number>(0);

    useEffect(() => {
        window.addEventListener('resize', setInnerWidth);
        setInnerWidth();

        return () => {
            window.removeEventListener('resize', setInnerWidth);
        }
    }, []);

    const setInnerWidth = () => {
        setWidth(window.innerWidth);
    }

    return width;
}