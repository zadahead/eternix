import React from 'react';
import './Inner.css';

const Inner: React.FC = (props) => {
    return (
        <div className={`Inner`}>
            {props.children}
        </div>
    )
}


export default Inner;