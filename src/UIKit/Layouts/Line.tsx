import React from 'react';
import './Line.css';

const Line: React.FC<Props> = (props) => {
    return (
        <div className={`Line ${props.className || ''}`}>
            {props.children}
        </div>
    )
}

export const Between: React.FC<Props> = (props) => {
    return <Line {...props} className="between" />
}

interface Props {
    className?: string
}

export default Line;