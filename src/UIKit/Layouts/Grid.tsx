import React from 'react';

import './Grid.css';

const Grid: React.FC<Props> = (props) => {
    return (
        <div className={`Grid ${props.className || ''}`}>
            {props.children}
        </div>
    )
}

export const GridRows: React.FC<Props> = (props) => {
    return <Grid {...props} className="rows" />
}

export const GridCols: React.FC<Props> = (props) => {
    return <Grid {...props} className="cols" />
}

interface Props {
    className?: string
}




export default Grid;