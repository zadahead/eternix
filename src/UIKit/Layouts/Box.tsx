
import React from "react";

import './Box.css';

const Box: React.FC = (props) => {
    return (
        <div className="Box">
            {props.children}
        </div>
    )
}

export default Box;