
import React, { useState, useEffect, useRef } from 'react';

import { Between, Icon } from "UIKit";
import './Dropdown.css';


const Dropdown: React.FC<Props> = (props) => {
    //state
    const [isOpen, setIsOpen] = useState(false);
    const wrapTag = useRef<HTMLDivElement>(null);

    //life cycle
    useEffect(() => {
        window.addEventListener('click', handleBodyClick);
        return () => {
            window.removeEventListener('click', handleBodyClick);
        }
    }, [])

    //handlers
    const handleToggleOpen = () => {
        setIsOpen(!isOpen);
    }

    const handleSelect = (item: any) => {
        props.onChange(item.id);

        handleClose();
    }

    const handleBodyClick = (e: any) => {
        console.log('handleBodyClick');

        if (!wrapTag.current?.contains(e.target)) {
            handleClose();
        }
    }

    const handleClose = () => {
        setIsOpen(false);
    }


    //render
    const renderHeader = () => {
        if (props.selected) {
            const selected = props.list.find(i => i.id === props.selected);
            if (selected) {
                return selected.value;
            }
        }
        return 'Please Select';
    }

    const renderList = () => {
        if (props.list) {
            return props.list.map(i => (
                <li key={i.id} onClick={() => { handleSelect(i) }}>{i.value}</li>
            ))
        }
    }

    return (
        <div className="Dropdown" ref={wrapTag}>
            <div className="header" onClick={handleToggleOpen}>
                <Between>
                    <p>{renderHeader()}</p>
                    <Icon i="chevron-down" />
                </Between>
            </div>
            {isOpen &&
                <ul className="list">
                    {renderList()}
                </ul>
            }
        </div>
    )
}

interface Props {
    list: { id: number, value: string }[];
    onChange: (id: number) => void;
    selected: number | null;
}

export default Dropdown;