
import { render, screen, fireEvent } from '@testing-library/react';

import Btn from './Btn';

describe('<Btn />', () => {
    //render
    describe('-> render', () => {
        it('will render component', () => {
            render(<Btn />);
        })
    })

    //props, states
    describe('-> props', () => {
        it('will render with children', () => {
            render(<Btn>Click Me</Btn>);

            screen.getByText('Click Me');
        })


        it('will render with icon', () => {
            render(<Btn i="heart">Click Me</Btn>);

            screen.getByText('Click Me');

            const icon = document.querySelectorAll('.fa-heart');
            expect(icon.length).toEqual(1);
        })
    })
    //events
    describe('-> events', () => {
        it('will trigger onClick', () => {
            //Assign
            const onClick = jest.fn();

            //Act
            render(<Btn onClick={onClick}>Click Me</Btn>);
            fireEvent.click(screen.getByText('Click Me'));

            //Assert
            expect(onClick).toBeCalledTimes(1);
        })
    })

    //lifecycle

    //handlers


})