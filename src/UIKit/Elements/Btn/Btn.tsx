import React from 'react';
import './Btn.css';

import { Between, Icon } from 'UIKit';

const Btn: React.FC<Props> = (props) => {

    const renderLine = () => {
        if (props.i) {
            return (
                <Between>
                    {props.children}
                    <Icon i={props.i} />
                </Between>
            )
        }
        return props.children;
    }


    return (
        <button className="Btn" id="abc" onClick={props.onClick}>
            {renderLine()}
        </button>
    )
}

interface Props {
    onClick: () => void;
    i?: string;
}
export default Btn;