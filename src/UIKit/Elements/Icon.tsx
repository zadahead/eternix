
import React from "react";


const Icon: React.FC<Props> = (props) => {
    return <i className={`fas fa-${props.i}`}></i>
}

interface Props {
    i: string
}
export default Icon;