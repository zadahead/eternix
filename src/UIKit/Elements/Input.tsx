import React from 'react';
import './Input.css';

const Input: React.FC<Props> = (props) => {
    const handleChange = (e: any) => {
        props.onChange(e.target.value);
    }

    return (
        <div className="Input">
            <input value={props.value} onChange={handleChange} />
        </div>
    )
}

interface Props {
    value: string,
    onChange: (value: string) => void
}

export default Input;