describe('Todos List', () => {
    beforeEach(() => {
        cy.intercept('GET', 'https://jsonplaceholder.typicode.com/todos', { fixture: 'todos.json' }).as('getTodos');
    })

    it('will visit page', () => {

        cy.visit('http://localhost:3000/todos');
        cy.wait('@getTodos');
    })

    it('will click on first item', () => {
        const firstItem = cy.get('.Box ul li').first();

        firstItem.should('have.css', 'text-decoration', 'none solid rgb(0, 0, 0)');
        firstItem.click();
        firstItem.should('have.css', 'text-decoration', 'line-through solid rgb(0, 0, 0)');
    })

    it('will write something', () => {
        cy.get('input').type('hello mosh');
    })
})